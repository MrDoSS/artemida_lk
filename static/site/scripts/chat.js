$(document).ready(function () {
    var msgArea = $('.chat')
    var elementMessage = $('#message')
    var ws = new WebSocket('ws://' + window.location.hostname + window.location.pathname);

    $(elementMessage).keyup(function (e) {
        if (e.which == 13 && !e.shiftKey) {
            sendMessage();
        } else if (e.which == 13 && e.shiftKey) {
            $(this).height($(this).height()+30);
        }
    });

    $('#btn-chat').click(function (e) {
        sendMessage();
    })

    function sendMessage() {
        var files = [];
        $(".file-item").each(function () {
            files.push(parseInt(this.id));
        });
        if (elementMessage.val()) {
            ws.send(JSON.stringify({'text': elementMessage.val(), 'files': files}));
            elementMessage.val('');
            $('.file-item').remove();
            $(elementMessage).height('auto');
        }
    }

    $('.message:last')[0].scrollIntoView();

    ws.onmessage = function (message) {
        var data = JSON.parse(message.data);
        var str = "";
        var files = JSON.parse(data['files']);
        for (var k in files) {
            str += '<div class="col-xs-12">\n' +
                '                                                    <a class="item r d-block" href="' + files[k] + '" download>\n' +
                '                                                        <div class="item-info">\n' +
                '                                                            <div class="item-title text-muted text-ellipsis">\n' +
                '                                                                <i class="material-icons">insert_drive_file</i>' + k + '\n' +
                '                                                            </div>\n' +
                '                                                        </div>\n' +
                '                                                    </a>\n' +
                '                                                </div>'
        }
        var newMessage = msgArea.append('<div class="message clearfix">\n' +
            '               <span class="chat-img pull-left">\n' +
            '                   <img src="' + data['avatar'] + '" alt="User Avatar" width="50" height="50" class="img-circle"/>\n' +
            '               </span>\n' +
            '               <div class="chat-body clearfix">\n' +
            '                   <div class="header">\n' +
            '                       <strong class="primary-font">' + data['user'] + '</strong>\n' +
            '                       <small class="pull-right text-muted">\n' +
            '                           ' + data['create_time'] + '\n' +
            '                       </small>\n' +
            '                   </div>\n' +
            '                   <p>' + data['message'].replace(/\n\r?/g, '<br />') + '</p>' +
            '                   <div class="row item-list item-list-xs">'
                                    + str +
            '                   </div>\n' +
            '               </div>\n' +
            '           </div>\n');
        $('.message:last')[0].scrollIntoView();
    };
});