(function ($) {
    $(document).on('click.site', '#search-result a', function (e) {
        $(this).closest('.modal').modal('hide');
    });

    $(document).on('click.site', '.audio-play', function (e) {
        var audioId = $(this).attr('data-id');
        var audio = $(audioId)[0];
        if ($(audio).hasClass('active')) {
            audio.pause();
            $(audio).removeClass('active');
            $(this).removeClass('is-playing');
        } else {
            $('audio').each(function () {
                this.pause();
                $(this).removeClass('active');
                $('.audio-play').removeClass('is-playing');
            })
            audio.play();
            $(audio).addClass('active');
            $(this).addClass('is-playing');
        }
    });

    $('audio').on('timeupdate', function () {
        playerTimeControl(this)
    });

})(jQuery);

function playerTimeControl(e) {
    var player = $(e)[0];
    var length = player.duration;
    var current_time = player.currentTime;

    // calculate total length of value
    var totalLength = calculateTotalValue(length)
    document.getElementById("end-time-" + player.id).innerHTML = totalLength;

    // calculate current value time
    var currentTime = calculateCurrentValue(current_time);
    document.getElementById("start-time-" + player.id).innerHTML = currentTime;

    var progressbar = document.getElementById('seek-obj-' + player.id);
    progressbar.value = (player.currentTime / player.duration);

    $(progressbar).click(function (e) {
        var percent = e.offsetX / this.offsetWidth;
        player.currentTime = percent * player.duration;
        progressbar.value = percent / 100;
    })
}

function calculateTotalValue(length) {
    var minutes = Math.floor(length / 60),
        seconds_int = length - minutes * 60,
        seconds_str = seconds_int.toString(),
        seconds = seconds_str.substr(0, 2),
        time = minutes + ':' + seconds

    return time;
}

function calculateCurrentValue(currentTime) {
    var current_hour = parseInt(currentTime / 3600) % 24,
        current_minute = parseInt(currentTime / 60) % 60,
        current_seconds_long = currentTime % 60,
        current_seconds = current_seconds_long.toFixed(),
        current_time = (current_minute < 10 ? "0" + current_minute : current_minute) + ":" + (current_seconds < 10 ? "0" + current_seconds : current_seconds);

    return current_time;
}