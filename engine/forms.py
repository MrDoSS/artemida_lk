from django import forms
from engine.models import GalleryImage, ServiceCustomer


class FormGallery(forms.ModelForm):
    class Meta:
        model = GalleryImage
        fields = ['image', ]

        # def __init__(self, *args, **kwargs):
        #     super(FormGallery, self).__init__(*args, **kwargs)
        #     for field in self.fields:
        #         self.fields[field].widget.attrs['class'] = 'form-control'


class ServiceCustomerForm(forms.ModelForm):
    class Meta:
        model = ServiceCustomer
        fields = ['title', 'image']

    def __init__(self, *args, **kwargs):
        super(ServiceCustomerForm, self).__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'form-control'
