from .models import AbstractMaterial
from django.contrib.contenttypes.admin import GenericStackedInline


class AttachmentInlines(GenericStackedInline):
    model = AbstractMaterial
    exclude = ()
    extra = 1