from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models


class MaterialManager(models.Manager):
    def materials_for_object(self, obj):
        object_type = ContentType.objects.get_for_model(obj)
        return self.filter(content_type__pk=object_type.id,
                           object_id=obj.pk)

    def videos_for_object(self, obj):
        object_type = ContentType.objects.get_for_model(obj)
        return self.filter(content_type__pk=object_type.id,
                           object_id=obj.pk, type=AbstractMaterial.TYPE.VIDEO)

    def audio_for_object(self, obj):
        object_type = ContentType.objects.get_for_model(obj)
        return self.filter(content_type__pk=object_type.id,
                           object_id=obj.pk, type=AbstractMaterial.TYPE.AUDIO)

    def images_for_object(self, obj):
        object_type = ContentType.objects.get_for_model(obj)
        return self.filter(content_type__pk=object_type.id,
                           object_id=obj.pk, type=AbstractMaterial.TYPE.IMAGE)

    def docs_for_object(self, obj):
        object_type = ContentType.objects.get_for_model(obj)
        return self.filter(content_type__pk=object_type.id,
                           object_id=obj.pk, type=AbstractMaterial.TYPE.DOC)


class AbstractMaterial(models.Model):
    class TYPE:
        VIDEO = 'Видео'
        AUDIO = 'Аудио'
        IMAGE = 'Изображение'
        DOC = 'Документ'

        CHOICES = (
            (VIDEO, 'Видео'),
            (AUDIO, 'Аудио'),
            (IMAGE, 'Изображение'),
            (DOC, 'Документ'),
        )

    objects = MaterialManager()
    name = models.CharField(max_length=255, verbose_name="Название")
    # attachment = models.FileField(verbose_name="Вложение", upload_to=lambda instance, filename: 'materials/{0}'.format(filename.encode('utf-8')))
    attachment = models.FileField(verbose_name="Вложение", upload_to='materials/', null=True, blank=True)
    attachment_link = models.CharField(max_length=1000, verbose_name="Ссылка на вложение", null=True, blank=True)
    type = models.CharField(max_length=255, choices=TYPE.CHOICES, verbose_name="Тип вложения")
    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')

    class Meta:
        verbose_name = 'материал'
        verbose_name_plural = "Материалы к семинару"

    def __str__(self):
        return self.name
