from django.core.mail import send_mail
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.template.loader import render_to_string
from django.urls import reverse

from config.settings import EMAIL_FOR_NOTIFICATIONS
from engine.accounts.models import User


class Chat(models.Model):
    users = models.ManyToManyField(User, verbose_name="Участники", blank=True)
    last_message = models.ForeignKey("engine.ChatMessage", related_name='last_message_chat',
                                     verbose_name="Последнее сообщение", null=True, blank=True)
    name = models.CharField(max_length=1000, verbose_name='Название чата', null=True, blank=True)

    def get_absolute_url(self):
        return reverse('chat', kwargs={'pk': self.pk})

    def __str__(self):
        return self.name

    # def save(self, force_insert=False, force_update=False, using=None,
    #          update_fields=None):
    #     self.name = ', '.join([user.get_full_name() for user in self.users.all()])
    #     super(Chat, self).save()

    class Meta:
        verbose_name = "чат"
        verbose_name_plural = "чаты"


class ChatMessage(models.Model):
    chat = models.ForeignKey(Chat)
    user = models.ForeignKey(User)
    message = models.TextField(verbose_name="Сообщение")
    create_time = models.DateTimeField(auto_now_add=True)

    def get_attachments(self):
        return Attachment.objects.filter(message=self)


class Attachment(models.Model):
    user = models.ForeignKey(User)
    message = models.ForeignKey(ChatMessage, null=True, blank=True)
    file = models.FileField(upload_to='chats/attachments/')


class ChatApplication(models.Model):
    chat = models.ForeignKey(Chat, verbose_name="Чат")
    user = models.ForeignKey(User, verbose_name="Пользователь")
    message = models.TextField(verbose_name="Сообщение", null=True, blank=True)

    class Meta:
        verbose_name = "заявку"
        verbose_name_plural = "Заявки для чата"

    def get_name(self):
        return "Чат: {0}\nПользователь: {1}".format(self.chat, self.user.get_full_name())


@receiver(post_save, sender=ChatApplication)
def new_chat_app_notification(sender, instance, created, **kwargs):
    if created:
        message = instance.get_name()
        html_message = render_to_string('engine/email/chat_app.html', {
            'chat': instance.chat, 'user': instance.user.get_full_name()
        })
        send_mail('Новая заявка на чат на сайте lk.artemida.mc"', message,
                  'Личный кабинет "Артемида" <artemida.lk@gmail.com>', (EMAIL_FOR_NOTIFICATIONS,), html_message=html_message)