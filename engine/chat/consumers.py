import json
from channels import Group
from channels.sessions import channel_session, enforce_ordering
from channels.auth import http_session_user, channel_session_user, channel_session_user_from_http
from django.utils import timezone
from django.template.defaultfilters import date as format_date
from engine.chat.models import Chat, ChatMessage, Attachment


@channel_session_user_from_http
def ws_add(message, room):
    message.reply_channel.send({"accept": True})
    message.channel_session['room'] = room
    Group('chat-%s' % room).add(message.reply_channel)


@channel_session_user
def ws_echo(message):
    room = message.channel_session['room']
    data = json.loads(message.content['text'])
    files = Attachment.objects.filter(id__in=data['files'])
    chat = Chat.objects.filter(id=room).first()
    create = ChatMessage.objects.create(chat=chat, user=message.user, message=data['text'])
    if create:
        chat.last_message = create
        chat.save()
        files_json = dict()
        for file in files:
            file.message = create
            file.save()
            files_json[file.file.name] = file.file.url
        create_time = format_date(timezone.localtime(create.create_time), 'j E o г. H:i')
        Group('chat-%s' % room).send({
            'text': json.dumps({'message': data['text'],
                                'user': message.user.get_full_name(),
                                'avatar': message.user.avatar.url,
                                'create_time': create_time,
                                'files': json.dumps(files_json)
                                })
        })
