from django import forms
from .models import Attachment, ChatApplication


class AttachForm(forms.ModelForm):
    class Meta:
        model = Attachment
        fields = ['file',]


class CreateChatApp(forms.ModelForm):
    class Meta:
        model = ChatApplication
        exclude = ['user']

    def __init__(self, chats, *args, **kwargs):
        super(CreateChatApp, self).__init__(*args, **kwargs)
        self.fields['chat'] = forms.ModelChoiceField(
            queryset=chats)
        self.fields['chat'].label = "Чат"
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'form-control'

