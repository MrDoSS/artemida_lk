from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, JsonResponse
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views import View
from django.views.generic import DetailView, CreateView, TemplateView
from engine.accounts.models import User
from engine.chat.forms import AttachForm, CreateChatApp
from engine.chat.models import Chat, ChatMessage, ChatApplication


class ChatView(DetailView):
    model = Chat
    template_name = "engine/chat.html"

    def dispatch(self, request, *args, **kwargs):
        if User.objects.filter(id=request.user.id, chat=self.get_object()):
            return super(ChatView, self).dispatch(request, *args, **kwargs)
        else:
            return HttpResponseRedirect(reverse('chat_app_create'))

    def get_context_data(self, **kwargs):
        context = super(ChatView, self).get_context_data(**kwargs)
        context['title'] = "Чат"
        context['messages'] = ChatMessage.objects.filter(chat=self.get_object()).order_by('create_time')
        return context


class ChatAppView(CreateView):
    model = ChatApplication
    form_class = CreateChatApp
    template_name = "engine/create_chat_app.html"
    success_url = "/chat-app/success/"
    chats = None

    @method_decorator(login_required())
    def dispatch(self, request, *args, **kwargs):
        self.chats = Chat.objects.exclude(users=request.user)
        return super(ChatAppView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ChatAppView, self).get_context_data(**kwargs)
        context['title'] = "Оставить заявку"
        return context

    def get_form(self, form_class=None):
        if form_class is None:
            form_class = self.get_form_class()
        return form_class(self.chats, **self.get_form_kwargs())


    def form_valid(self, form):
        form.instance.user = self.request.user
        return super(ChatAppView, self).form_valid(form)


class ChatAppSuccess(TemplateView):
    template_name = "engine/success_create_chat_app.html"

    def get_context_data(self, **kwargs):
        context = super(ChatAppSuccess, self).get_context_data(**kwargs)
        context['title'] = "Заявка успешно отправлена"
        return context


class ChatFileUpload(View):
    def post(self, request):
        form = AttachForm(self.request.POST, self.request.FILES)
        if form.is_valid():
            instance = form.save(commit=False)
            instance.user = request.user
            instance.save()
            data = {'is_valid': True, 'id': instance.id, 'url': instance.file.url, 'filename': instance.file.name}
        else:
            data = {'is_valid': False}
        return JsonResponse(data)
