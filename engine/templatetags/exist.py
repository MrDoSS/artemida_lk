from django import template

from engine.accounts.models import User, PersonalPage

register = template.Library()


@register.simple_tag(takes_context=True)
def testimonial_exist(context, value):
    request = context['request']
    return "checked" if PersonalPage.objects.filter(user__id=request.user.id, testimonials=value) else ""


@register.simple_tag(takes_context=True)
def article_exist(context, value):
    request = context['request']
    return "checked" if PersonalPage.objects.filter(user__id=request.user.id, articles=value) else ""


@register.filter(name='field_type')
def field_type(field):
    return field.field.widget.__class__.__name__