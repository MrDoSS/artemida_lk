from ckeditor.widgets import CKEditorWidget
from django import forms
from django.contrib import auth
from .models import User, PersonalPage, Education
from engine.models import City
from registration.forms import RegistrationFormUniqueEmail
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm


class CustomRegForm(UserCreationForm):
    city_label =forms.CharField()

    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email', 'phone', 'profession', 'specialization',
                  'work', 'city_label', 'city', 'birthday', 'avatar', 'password1', 'password2', 'is_private']
        widgets = {
            'city': forms.TextInput(attrs={'type': 'hidden'}),
        }

    def __init__(self, *args, **kwargs):
        super(CustomRegForm, self).__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'form-control'
            self.fields[field].widget.attrs['placeholder'] = self.fields[field].help_text
        self.fields['first_name'].widget.attrs['placeholder'] = "Пример: Иван"
        self.fields['last_name'].widget.attrs['placeholder'] = "Пример: Иванов"
        self.fields['username'].widget.attrs['placeholder'] = "Пример: ivan1992 "
        self.fields['specialization'].widget.attrs['id'] = "reg_specialization"
        self.fields['email'].widget.attrs['placeholder'] = "Пример: ivan@mail.ru"
        self.fields['work'].widget.attrs['placeholder'] = "Введите место своей работы"
        self.fields['city_label'].widget.attrs['placeholder'] = "Пример: Ростов-на-Дону"
        self.fields['password1'].widget.attrs[
            'placeholder'] = "Ваш пароль не должен совпадать с вашим именем или другой персональной информацией или быть слишком похожим на неё."
        self.fields['city_label'].label = "Город"



class EduForm(forms.ModelForm):
    class Meta:
        model = Education
        fields = ['edu']
        exclude = ['user', 'id']
        widgets = {
            'edu': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Пример: ЮФУ ИВТиПТ', 'required':True}),
        }


class CustomLoginForm(AuthenticationForm):
    def __init__(self, *args, **kwargs):
        super(CustomLoginForm, self).__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'form-control'


class AccountEditForm(forms.ModelForm):
    city_label =forms.CharField()

    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email', 'phone', 'profession', 'specialization', 'work', 'city_label',
                  'city', 'birthday', 'passport_scan', 'diploma_scan', 'avatar', 'is_private']
        widgets = {
            'city': forms.TextInput(attrs={'type':'hidden'}),
        }

    def __init__(self, *args, **kwargs):
        super(AccountEditForm, self).__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'form-control'
        self.fields['work'].widget.attrs['placeholder'] = "Введите место своей работы"
        self.fields['city_label'].label = "Город"


class CreatePersonalPageForm(forms.ModelForm):
    class Meta:
        model = PersonalPage
        exclude = ['user', 'page_url', 'is_active', 'testimonials', 'articles']
        widgets = {
            'information': CKEditorWidget,
            'contacts_or_link': forms.RadioSelect()
        }

    def __init__(self, *args, **kwargs):
        super(CreatePersonalPageForm, self).__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'form-control'


class TestimonialsForm(forms.ModelForm):
    class Meta:
        model = PersonalPage
        fields = ['testimonials',]
        widgets = {'testimonials': forms.CheckboxSelectMultiple}


class ArticlesForm(forms.ModelForm):
    class Meta:
        model = PersonalPage
        fields = ['articles']
        widgets = {'articles': forms.CheckboxSelectMultiple}
