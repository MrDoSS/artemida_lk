from decimal import *

import requests
from django.contrib import auth, messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import PasswordResetView
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ObjectDoesNotExist
from django.db import transaction
from django.forms import inlineformset_factory
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.urls import reverse, reverse_lazy
from django.utils.decorators import method_decorator
from django.views import View
from django.views.generic import CreateView, TemplateView, DetailView, UpdateView, ListView
from config import settings
from config.settings import TINKOFF_TERMINAL_ID
from engine.accounts.models import PersonalPage, Education, User, PersonalPageApplication
from engine.chat.models import Chat
from engine.forms import FormGallery, ServiceCustomerForm
from engine.utils import is_mine
from .forms import CreatePersonalPageForm, AccountEditForm, CustomRegForm, EduForm, TestimonialsForm, ArticlesForm
from engine.models import GalleryImage, Seminar, Mailing, ServiceCustomer, Testimonial, Article, Order


class RegView(CreateView):
    model = settings.AUTH_USER_MODEL
    form_class = CustomRegForm
    template_name = 'registration/registration_form.html'
    success_url = '/login'
    EduFormset = inlineformset_factory(User, Education, form=EduForm, can_delete=False, extra=1)

    def get_context_data(self, **kwargs):
        context = super(RegView, self).get_context_data(**kwargs)
        context['title'] = 'Регистрация'
        context['formset'] = self.EduFormset()
        return context

    def form_valid(self, form):
        formset = self.EduFormset(self.request.POST)

        with transaction.atomic():
            self.object = form.save()
            if formset.is_valid():
                formset.instance = self.object
                formset.save()

        messages.success(self.request,
                         'Вы успешно зарегистрировались! Ваши данные отправлены на рассмотрение и после подтверждения вам придет уведомление на почту. Рекомендуем также проверить папку "спам"')
        return super(RegView, self).form_valid(form)


class AccountView(UpdateView):
    template_name = "engine/account.html"
    model = settings.AUTH_USER_MODEL
    form_class = AccountEditForm
    testimonials = TestimonialsForm
    articles = ArticlesForm
    success_url = "."
    EduFormset = inlineformset_factory(User, Education, form=EduForm, can_delete=False, extra=0)

    @method_decorator(login_required())
    def dispatch(self, request, *args, **kwargs):
        return super(AccountView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(AccountView, self).get_context_data(**kwargs)
        context['title'] = "Личный кабинет"
        context['seminars'] = Seminar.objects.filter(users=self.get_object())
        context['formset'] = self.EduFormset(instance=self.get_object())
        context['chats'] = Chat.objects.all().order_by('-last_message')
        context['news'] = Mailing.objects.filter(users=self.request.user).order_by('-create')
        context['testimonials'] = Testimonial.objects.all()
        context['articles'] = Article.objects.all()
        return context

    def get_object(self, queryset=None):
        return self.request.user

    def form_valid(self, form):
        formset = self.EduFormset(self.request.POST, instance=self.object)
        testimonials = self.testimonials(self.request.POST, instance=self.object)
        articles = self.articles(self.request.POST, instance=self.object)
        if formset.is_valid():
            formset.save()
        if testimonials.is_valid():
            testimonials.save()
        if articles.is_valid():
            articles.save()
        return super(AccountView, self).form_valid(form)


class ResetPasswordView(PasswordResetView):
    from_email = 'Личный кабинет "Артемида" <artemida.lk@gmail.com>'


class PersonalUserPage(DetailView, UpdateView):
    model = PersonalPage
    template_name = "engine/personalpage_detail.html"
    context_object_name = 'personal_page'
    testimonials = TestimonialsForm
    articles = ArticlesForm
    form_class = CreatePersonalPageForm
    ServiceFormset = inlineformset_factory(PersonalPage, ServiceCustomer, form=ServiceCustomerForm, can_delete=False,
                                           extra=0)

    def dispatch(self, request, *args, **kwargs):
        if PersonalPageApplication.objects.filter(user=auth.get_user(request), created=False):
            return HttpResponseRedirect(reverse("page_help_not_create"))
        obj = self.get_object()
        if not is_mine(request, obj):
            return HttpResponseRedirect("/")
        elif not self.get_object().is_active:
            self.template_name = "engine/page_not_active.html"
        return super(PersonalUserPage, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(PersonalUserPage, self).get_context_data(**kwargs)
        context['title'] = self.get_object()
        context['gallery_images'] = GalleryImage.objects.filter(personal_page=self.get_object())
        context['edu'] = Education.objects.filter(user=self.get_object().user)
        context['formset'] = self.ServiceFormset(instance=self.get_object())
        context['testimonials'] = Testimonial.objects.all()
        context['articles'] = Article.objects.all()
        return context

    def get_object(self, queryset=None):
        return PersonalPage.objects.filter(user=self.request.user).first()

    def form_valid(self, form):
        if is_mine(self.request, self.get_object()):
            formset = self.ServiceFormset(self.request.POST, self.request.FILES, instance=self.object)
            testimonials = self.testimonials(self.request.POST, instance=self.object)
            articles = self.articles(self.request.POST, instance=self.object)
            if formset.is_valid():
                formset.save()
            if testimonials.is_valid():
                testimonials.save()
            if articles.is_valid():
                articles.save()
            return super(PersonalUserPage, self).form_valid(form)
        else:
            return HttpResponseRedirect('/')


class ViewPersonalPage(TemplateView):
    template_name = 'engine/view_personal_page.html'

    @method_decorator(login_required())
    def dispatch(self, request, *args, **kwargs):
        personal_page = PersonalPage.objects.filter(user=auth.get_user(request)).first()
        if personal_page:
            return HttpResponseRedirect(reverse("personal_page"))
        elif PersonalPageApplication.objects.filter(user=auth.get_user(request), created=False):
            return HttpResponseRedirect(reverse("page_help_not_create"))
        else:
            return super(ViewPersonalPage, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ViewPersonalPage, self).get_context_data(**kwargs)
        context['title'] = 'Персональная страница'
        return context


class CreatePersonalPage(CreateView):
    model = PersonalPage
    form_class = CreatePersonalPageForm
    template_name = 'engine/create_personal_page.html'
    personal_page = None
    ServiceFormset = inlineformset_factory(PersonalPage, ServiceCustomer, form=ServiceCustomerForm, can_delete=False,
                                           extra=1)
    testimonials = TestimonialsForm
    articles = ArticlesForm
    success_url = reverse_lazy('personal_page_payment')

    @method_decorator(login_required())
    def dispatch(self, request, *args, **kwargs):
        self.personal_page = PersonalPage.objects.filter(user=auth.get_user(request)).first()
        if not self.personal_page:
            return super(CreatePersonalPage, self).dispatch(request, *args, **kwargs)
        elif PersonalPageApplication.objects.filter(user=auth.get_user(request)):
            return HttpResponseRedirect(reverse("page_help_not_create"))
        else:
            return HttpResponseRedirect(self.personal_page.get_absolute_url())

    def form_valid(self, form):
        form.instance.user = auth.get_user(self.request)
        formset = self.ServiceFormset(self.request.POST, self.request.FILES)
        testimonials = self.testimonials(self.request.POST)
        articles = self.articles(self.request.POST)
        with transaction.atomic():
            self.object = form.save()

            if formset.is_valid():
                formset.instance = self.object
                formset.save()
            if testimonials.is_valid():
                testimonials.instance = self.object
                testimonials.save()
            if articles.is_valid():
                articles.instance = self.object
                articles.save()
        return super(CreatePersonalPage, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(CreatePersonalPage, self).get_context_data(**kwargs)
        context['title'] = 'Создать персональную страницу'
        context['formset'] = self.ServiceFormset()
        context['testimonials'] = Testimonial.objects.all()
        context['articles'] = Article.objects.all()
        return context


class LandingCustomer(DetailView):
    model = PersonalPage
    template_name = "engine/customer/base.html"
    context_object_name = "landing"

    def dispatch(self, request, *args, **kwargs):
        if not self.get_object().is_active:
            self.template_name = "engine/page_not_active.html"
        return super(LandingCustomer, self).dispatch(request, *args, **kwargs)

    def get_object(self, queryset=None):
        return get_object_or_404(PersonalPage, page_url=self.kwargs.get("page_url"))

    def get_context_data(self, **kwargs):
        context = super(LandingCustomer, self).get_context_data(**kwargs)
        context['title'] = self.object.title
        context['services'] = ServiceCustomer.objects.filter(personal_page=self.object)
        return context


class HelpCreatePersonalPage(TemplateView):
    template_name = 'engine/help_create_pp.html'

    def get_context_data(self, **kwargs):
        context = super(HelpCreatePersonalPage, self).get_context_data(**kwargs)
        context['title'] = 'Помощь в создании персональной страницы'
        return context


class PersonalPageHelpNotCreate(TemplateView):
    template_name = 'engine/page_help_not_create.html'

    def get_context_data(self, **kwargs):
        context = super(PersonalPageHelpNotCreate, self).get_context_data(**kwargs)
        context['title'] = 'Вы подали заявку на создание персональной страницы.'
        return context


class PaymentView(View):
    order = None
    personal_page = None

    def dispatch(self, request, *args, **kwargs):
        if not PersonalPageApplication.objects.filter(user=auth.get_user(request)):
            try:
                self.personal_page = PersonalPage.objects.get(user=auth.get_user(request))
                if not self.personal_page.is_active:
                    return super(PaymentView, self).dispatch(request)
                return HttpResponseRedirect('/')
            except ObjectDoesNotExist:
                return HttpResponseRedirect('/')
        return HttpResponseRedirect('/')

    def get(self, request, *args, **kwargs):
        try:
            old_order = Order.objects.get(user=self.personal_page.user, object_id=self.personal_page.id)
            old_order.delete()
        except ObjectDoesNotExist:
            pass

        self.order = Order.objects.create(
            user=self.personal_page.user,
            content_type=ContentType.objects.get_for_model(PersonalPage),
            object_id=self.personal_page.id,
            amount=3000,
        )

        req_data = {
            'TerminalKey': TINKOFF_TERMINAL_ID,
            'Amount': int(self.order.amount * 100),
            'OrderId': self.order.id,
        }

        response = requests.post('https://securepay.tinkoff.ru/v2/Init', json=req_data)

        return HttpResponseRedirect(response.json().get('PaymentURL', '/'))


class PaymentAppView(View):
    order = None
    personal_page_app = None

    def dispatch(self, request, *args, **kwargs):
        if not PersonalPage.objects.filter(user=auth.get_user(request)):
            try:
                self.personal_page_app = PersonalPageApplication.objects.get(user=auth.get_user(request))
                if not self.personal_page_app.is_payment:
                    return super(PaymentAppView, self).dispatch(request)
                return HttpResponseRedirect('/')
            except ObjectDoesNotExist:
                self.personal_page_app = PersonalPageApplication.objects.create(user=auth.get_user(request))
                return super(PaymentAppView, self).dispatch(request)
        return HttpResponseRedirect('/')

    def get(self, request, *args, **kwargs):
        try:
            old_order = Order.objects.get(user=self.personal_page_app.user, object_id=self.personal_page_app.id)
            old_order.delete()
        except ObjectDoesNotExist:
            pass

        self.order = Order.objects.create(
            user=self.personal_page_app.user,
            content_type=ContentType.objects.get_for_model(PersonalPageApplication),
            object_id=self.personal_page_app.id,
            amount=4000,
        )

        req_data = {
            'TerminalKey': TINKOFF_TERMINAL_ID,
            'Amount': int(self.order.amount * 100),
            'OrderId': self.order.id,
        }

        response = requests.post('https://securepay.tinkoff.ru/v2/Init', json=req_data)
        return HttpResponseRedirect(response.json().get('PaymentURL', '/'))

