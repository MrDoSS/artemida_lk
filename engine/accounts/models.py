from django.contrib.auth.models import AbstractUser
from django.contrib.auth.validators import UnicodeUsernameValidator
from django.contrib.contenttypes.fields import GenericRelation
from django.core.mail import send_mail
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.template.loader import render_to_string
from django.urls import reverse
from django_resized import ResizedImageField
from config import settings
from ckeditor.fields import RichTextField
from django.utils.translation import gettext_lazy as _
import cyrtranslit

from config.settings import EMAIL_FOR_NOTIFICATIONS
from model_utils import FieldTracker


class User(AbstractUser):
    class PROFESSION:
        DOCTOR = 'Врач'
        PSYCHOLOGIST = 'Психолог'

        CHOICES = (
            (DOCTOR, 'Врач'),
            (PSYCHOLOGIST, 'Психолог'),
        )

    username_validator = UnicodeUsernameValidator()

    username = models.CharField(
        _('Имя пользователя (логин)'),
        max_length=150,
        unique=True,
        help_text=_('Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.'),
        validators=[username_validator],
        error_messages={
            'unique': _("A user with that username already exists."),
        },
    )
    email = models.EmailField('E-mail', blank=True, null=True)
    patronymic = models.CharField(max_length=255, verbose_name='Отчество', null=True, blank=True)
    phone = models.CharField(max_length=255, verbose_name="Телефон", unique=True,
                             help_text="Пример: +7 (928) 123-45-67", default="")
    birthday = models.DateField(verbose_name="Дата рожения", help_text="Пример: 01.01.1987", null=True, blank=True)
    city = models.ForeignKey('engine.City', verbose_name="Город", help_text="Пример: Ростов-на-Дону")
    avatar = ResizedImageField(size=[250, 250], quality=100, verbose_name="Аватар",
                               upload_to='avatars/', default="")
    passport_scan = models.FileField(verbose_name="Скан паспорта",
                                     upload_to='scans/passports/', null=True, blank=True)
    diploma_scan = models.FileField(verbose_name="Скан диплома",
                                    upload_to='scans/diplomas/', null=True, blank=True)
    profession = models.CharField(max_length=255, choices=PROFESSION.CHOICES, verbose_name="Профессия", null=True,
                                  blank=True)
    specialization = models.CharField(max_length=255, verbose_name="Специализация")
    work = models.CharField(max_length=255, verbose_name="Место работы", null=True, blank=True)
    is_private = models.BooleanField(verbose_name="Приватная страница", default=False, help_text=_(
        'Страница, которая не видна на карте врачей'
    ))
    is_active = models.BooleanField(
        _('active'),
        default=False,
        help_text=_(
            'Designates whether this user should be treated as active. '
            'Unselect this instead of deleting accounts.'
        ),
    )

    REQUIRED_FIELDS = ['first_name', 'last_name']

    tracker = FieldTracker()

    def __str__(self):
        return self.get_full_name()

    def clean(self):
        if self.email == "":
            self.email = None

    def get_full_name(self):
        if self.patronymic:
            return "%s %s %s" % (self.last_name, self.first_name, self.patronymic)
        else:
            return "%s %s" % (self.last_name, self.first_name)

    def get_personal_page(self):
        return PersonalPage.objects.filter(user=self, is_active=True).first() if not None else None

    class Meta:
        unique_together = ('email',)
        verbose_name = 'пользователь'
        verbose_name_plural = 'пользователи'


@receiver(post_save, sender=User)
def new_reg_notification(sender, instance, created, **kwargs):
    if created:
        message = 'ФИО: %s\n' % (instance.get_full_name())
        send_mail('Новая регистрация на сайте lk.artemida.mc"', message,
                  'Личный кабинет "Артемида" <artemida.lk@gmail.com>', (EMAIL_FOR_NOTIFICATIONS,), html_message=message)


@receiver(post_save, sender=User)
def user_activated_notification(sender, instance, created, **kwargs):
    if instance.tracker.has_changed('is_active') and instance.is_active and instance.email:
        message = 'Добрый день, {0}. Ваш аккаунт на сайте успешно активирован. Вы можете войти в свой личный кабинет'.format(instance.username)
        html_message = render_to_string('engine/email/user_activated.html', {
            'username': instance.username
        })
        send_mail('Активация аккаунта на сайте lk.artemida.mc"', message,
                  'Личный кабинет "Артемида" <artemida.lk@gmail.com>', (instance.email,), html_message=html_message)


class PersonalPage(models.Model):
    class SHOW:
        BUTTON = 'button'
        CONTACTS = 'contacts'

        CHOICES = (
            (BUTTON, 'Кнопка "Записаться"'),
            (CONTACTS, 'Контактные данные'),
        )

    user = models.OneToOneField(settings.AUTH_USER_MODEL, verbose_name="Пользователь", on_delete=models.CASCADE)
    title = models.CharField(max_length=1000, verbose_name="Заголовок страницы", null=True)
    banner = ResizedImageField(verbose_name="Верхний баннер",
                               upload_to='landing/banners/', null=True)
    about_img = ResizedImageField(verbose_name='Фото для блока "О себе"',
                                  upload_to='landing/photo/', null=True)
    information = RichTextField(verbose_name="Информация о себе", null=True)
    enroll_link = models.CharField(max_length=255, verbose_name='Ссылка для кнопки "Записаться"', null=True, blank=True)
    enroll_text = models.CharField(max_length=255, verbose_name='Текс для кнопки "Записаться"', default='Записаться на прием')
    contacts_or_link = models.CharField(max_length=100, verbose_name='Что отображать', default=SHOW.CONTACTS,
                                        choices=SHOW.CHOICES)
    testimonials = models.ManyToManyField("engine.Testimonial", verbose_name="Отзывы", blank=True)
    articles = models.ManyToManyField("engine.Article", verbose_name="Статьи", blank=True)
    page_url = models.CharField(max_length=15, verbose_name="URL страницы", unique=True, null=True)
    is_active = models.BooleanField(verbose_name="Активная страница", default=False)

    orders = GenericRelation('engine.Order')

    def __str__(self):
        return "Страница пользователя %s" % self.user.get_full_name()

    def get_absolute_url(self):
        return reverse('landing', kwargs={'page_url': self.page_url})

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.page_url = cyrtranslit.to_latin(self.user.username.lower())
        return super(PersonalPage, self).save()

    class Meta:
        verbose_name = "персональную странцу"
        verbose_name_plural = "Персональные страницы"


class PersonalPageApplication(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, verbose_name="Пользователь", on_delete=models.CASCADE)
    is_payment = models.BooleanField(verbose_name='Оплачено', default=False)
    created = models.BooleanField(verbose_name='Страница создана', default=False)

    def __str__(self):
        return "Заявка на создание персональной страницы пользователя %s" % self.user.get_full_name()


class Education(models.Model):
    user = models.ForeignKey(User, verbose_name="Пользователь")
    edu = models.CharField(max_length=255, verbose_name="Образование", null=True, blank=True)
