from cities_light.abstract_models import AbstractCity, AbstractRegion, AbstractCountry
from ckeditor.fields import RichTextField
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from django.contrib.contenttypes.models import ContentType
from django.core.mail import send_mail
from django.db import models
from django.db.models.signals import m2m_changed
from django.dispatch import receiver
from django.template.loader import render_to_string
from django.urls import reverse
from django_resized import ResizedImageField
from config import settings
from engine.accounts.models import PersonalPage, User


class Category(models.Model):
    name = models.CharField(max_length=500, verbose_name="Название", unique=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "категория"
        verbose_name_plural = "категории"


class Seminar(models.Model):
    name = models.CharField(max_length=500, verbose_name="Название")
    slug = models.SlugField(verbose_name="Ярлык")
    category = models.ForeignKey(Category, on_delete=models.CASCADE, verbose_name='Категория', null=True)
    short_desc = RichTextField(verbose_name="Краткое описание", null=True, blank=True)
    description = RichTextField(verbose_name="Описание")
    image = ResizedImageField(verbose_name="Миниатюра", upload_to='seminars/', null=True, blank=True)
    users = models.ManyToManyField(settings.AUTH_USER_MODEL, verbose_name="Пользователи", blank=True)
    amount = models.FloatField(verbose_name='Стоимость', null=True, blank=True)
    create = models.DateField(verbose_name='Дата семинара')

    class Meta:
        verbose_name = "семинар"
        verbose_name_plural = "семинары"
        ordering = ['-create']

    def __str__(self):
        return self.name

    def natural_key(self):
        return (self.name,)

    def get_absolute_url(self):
        return reverse('seminar_detail', kwargs={'slug': self.slug})

    def get_preview_url(self):
        return reverse('seminar_preview', kwargs={'slug': self.slug})

    def get_payment_url(self):
        return reverse('seminar_payment', kwargs={'slug': self.slug})


class GalleryImage(models.Model):
    personal_page = models.ForeignKey(PersonalPage, verbose_name="Пользователь")
    # image = ResizedImageField(verbose_name="Изображение", upload_to=lambda instance, filename: 'galleries/{0}'.format(filename.encode('utf-8')), null=True, blank=True)
    image = ResizedImageField(verbose_name="Изображение", upload_to='galleries/', null=True, blank=True)


class Mailing(models.Model):
    users = models.ManyToManyField(settings.AUTH_USER_MODEL, verbose_name="Получатели")
    theme = models.CharField(max_length=255, verbose_name='Тема письма')
    text = RichTextField(verbose_name="Текст письма")
    create = models.DateTimeField(auto_created=True, auto_now_add=True)

    class Meta:
        verbose_name = 'рассылку'
        verbose_name_plural = 'почтовые рассылки'


@receiver(m2m_changed, sender=Mailing.users.through)
def send_news(sender, instance, action, **kwargs):
    if action == 'post_add':
        recipient_list = instance.users.values_list('email', flat=True)
        for email in recipient_list:
            # send_mail(instance.theme, instance.text,
            #           'Личный кабинет "Артемида" <artemida.lk@gmail.com>', (email,), html_message=instance.text)
            html_message = render_to_string('engine/send_email.html')
            txt_message = 'Добрый день! У вас новое сообщение. Чтобы прочитать его, перейдите в личный кабинет: ' \
                          'http://lk.artemida-mc.ru/login/ '
            send_mail('Новое сообщение в личном кабенете "Артемида"', txt_message,
                      'Личный кабинет "Артемида" <artemida.lk@gmail.com>', (email,), html_message=html_message)


class ServiceCustomer(models.Model):
    personal_page = models.ForeignKey(PersonalPage, verbose_name="Пользователь")
    title = models.CharField(max_length=255, verbose_name="Название")
    image = ResizedImageField(verbose_name="Изображение", upload_to='landing/services/')


class Testimonial(models.Model):
    author = models.CharField(max_length=255, verbose_name="Автор отзыва")
    text = RichTextField(verbose_name="Текст отзыва")

    class Meta:
        verbose_name = 'отзыв'
        verbose_name_plural = 'отзывы'

    def __str__(self):
        return self.author


class Article(models.Model):
    author = models.CharField(max_length=500, verbose_name="Автор")
    title = models.CharField(max_length=500, verbose_name="Название")
    text = RichTextField(verbose_name="Текст")

    class Meta:
        verbose_name = 'статью'
        verbose_name_plural = 'статьи'

    def __str__(self):
        return self.title


class Order(models.Model):
    AUTHORIZED = 'AUTHORIZED'
    CONFIRMED = 'CONFIRMED'
    REVERSED = 'REVERSED'
    REJECTED = 'REJECTED'
    REFUNDED = 'REFUNDED'

    CHOICES = ((AUTHORIZED, 'Ожидание'),
               (CONFIRMED, 'Успех'),
               (REJECTED, 'Ошибка'),
               (REFUNDED, 'Возврат'),
               (REVERSED, 'Отмена'))

    user = models.ForeignKey(User, verbose_name='Пользователь', null=True)
    amount = models.FloatField(verbose_name='Сумма', null=True)
    status = models.CharField(max_length=255, verbose_name='Статус', choices=CHOICES, default=AUTHORIZED)
    created = models.DateTimeField(verbose_name='Дата создания', auto_now_add=True)

    content_type = models.ForeignKey(ContentType, null=True)
    object_id = models.PositiveIntegerField(null=True)
    content_object = GenericForeignKey('content_type', 'object_id')

    def get_amount(self):
        return round(self.amount)


class City(AbstractCity):
    def get_display_name(self):
        if self.region_id:
            return '%s, %s' % (self.alternate_names, self.region.alternate_names)
        else:
            return self.alternate_names

    def __str__(self):
        if self.region_id:
            return '%s, %s' % (self.alternate_names, self.region.alternate_names)
        else:
            return self.alternate_names

    def get_users(self):
        return User.objects.filter(city=self, is_staff=False, is_private=False)


class Region(AbstractRegion):
    def get_display_name(self):
        return self.alternate_names


class Country(AbstractCountry):
    pass


class PrepaymentEvent(models.Model):
    title = models.CharField(max_length=500, verbose_name="Название")
    text = RichTextField(verbose_name="Текст", null=True, blank=True)
    amount = models.FloatField(verbose_name='Стоимость')
    create = models.DateField(verbose_name='Дата события')

    def get_amount(self):
        return round(self.amount)

    class Meta:
        verbose_name = "Событие"
        verbose_name_plural = "События"
        ordering = ['-create']

    def __str__(self):
        return self.title


class Software(models.Model):
    id_device = models.IntegerField(verbose_name='ID устройства', unique=True)
    is_payment = models.BooleanField(verbose_name='Оплачено', default=False)
    email = models.EmailField(verbose_name='Email', null=True)

    class Meta:
        verbose_name = 'Программное обеспечение'
        verbose_name_plural = 'Программное обеспечение'

    def __str__(self):
        return 'Оборудование #%s' % self.id_device

    @staticmethod
    def get_amount():
        return 6000

    @staticmethod
    def get_service_name():
        return 'ПО "Активационная терапия"'
