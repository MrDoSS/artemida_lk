import hashlib
import json
import pyodbc

import requests

from django.contrib import auth, messages
from django.contrib.auth.decorators import login_required
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.shortcuts import get_object_or_404
from django.utils.decorators import method_decorator
from django.views import View
from django.views.decorators.clickjacking import xframe_options_exempt
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import DetailView, ListView, TemplateView

from engine.forms import FormGallery
from engine.search.forms import SearchCities
from engine.utils import is_mine
from .models import Seminar, Order, Category, City, PrepaymentEvent, Software
from .accounts.models import PersonalPage, User, PersonalPageApplication
from .materials.models import AbstractMaterial

from config.settings import TINKOFF_TERMINAL_ID, PYODBC_CONNECT


class FrontPage(TemplateView):
    template_name = 'engine/frontpage.html'

    def get_context_data(self, **kwargs):
        context = super(FrontPage, self).get_context_data(**kwargs)
        context['title'] = "Главная страница"
        return context


class SeminarsView(ListView):
    model = Seminar
    template_name = 'engine/seminars.html'

    def get_context_data(self, **kwargs):
        context = super(SeminarsView, self).get_context_data(**kwargs)
        context['title'] = "Семинары"
        context['categories'] = Category.objects.all()
        return context


class SeminarDetail(DetailView):
    model = Seminar

    @method_decorator(login_required())
    def dispatch(self, request, *args, **kwargs):
        if Seminar.objects.filter(pk=self.get_object().pk, users=auth.get_user(request)):
            return super(SeminarDetail, self).dispatch(request, *args, **kwargs)
        else:
            return HttpResponseRedirect(self.get_object().get_preview_url())

    def get_context_data(self, **kwargs):
        context = super(SeminarDetail, self).get_context_data(**kwargs)
        context['title'] = self.object.name
        context['all_attach'] = AbstractMaterial.objects.materials_for_object(self.get_object())
        context['videos'] = AbstractMaterial.objects.videos_for_object(self.get_object())
        context['audios'] = AbstractMaterial.objects.audio_for_object(self.get_object())
        context['images'] = AbstractMaterial.objects.images_for_object(self.get_object())
        context['docs'] = AbstractMaterial.objects.docs_for_object(self.get_object())
        return context


class SeminarPreview(DetailView):
    model = Seminar
    template_name = 'engine/seminar_preview.html'

    def get_context_data(self, **kwargs):
        context = super(SeminarPreview, self).get_context_data(**kwargs)
        context['title'] = self.object.name
        return context


class SuccessView(TemplateView):
    template_name = 'engine/success.html'

    def get_context_data(self, **kwargs):
        context = super(SuccessView, self).get_context_data(**kwargs)
        context['title'] = "Оплата прошла успешено"
        return context

        # def get(self, request, *args, **kwargs):
        #     id = request.GET.get('seminar-id')
        #     seminar = Seminar.objects.filter(id=id).first()
        #     seminar.users.add(auth.get_user(request))
        #     return super(SuccessView, self).get(request, *args, **kwargs)


class DoctorsList(ListView):
    model = User
    context_object_name = 'doctors'
    template_name = 'engine/doctors_list.html'

    def get_queryset(self):
        return User.objects.filter(is_private=False, is_staff=False)

    def get_context_data(self, **kwargs):
        context = super(DoctorsList, self).get_context_data(**kwargs)
        context['title'] = 'Наши специалисты'
        context['seatch_form'] = SearchCities
        return context


class FAQView(TemplateView):
    template_name = "engine/faq.html"

    def get_context_data(self, **kwargs):
        context = super(FAQView, self).get_context_data(**kwargs)
        context['title'] = 'Вопросы и ответы'
        return context


def get_user_city(request):
    if request.user.is_authenticated:
        city = auth.get_user(request).city.__str__()
        return JsonResponse({"city": city})


def get_cities(request):
    if request.is_ajax():
        q = request.GET.get('term', '')
        cities = City.objects.filter(alternate_names__icontains=q)[:20]
        results = []
        for city in cities:
            city_json = {}
            name = city.get_display_name
            city_json['label'] = city.__str__()
            city_json['value'] = city.id
            results.append(city_json)
        data = json.dumps(results)
    else:
        data = 'fail'
    mimetype = 'application/json'
    return HttpResponse(data, mimetype)


class GalleryFileUpload(View):
    def post(self, request):
        personal_page = PersonalPage.objects.filter(user=auth.get_user(request)).first()
        if is_mine(request, personal_page):
            form = FormGallery(self.request.POST, self.request.FILES)
            if form.is_valid():
                instance = form.save(commit=False)
                instance.personal_page = personal_page
                instance.save()
                data = {'is_valid': True, 'id': instance.id, 'url': instance.image.url}
            else:
                data = {'is_valid': False}
            return JsonResponse(data)


class APIDoctors(ListView):
    model = User
    context_object_name = 'doctors'
    template_name = 'api/doctors.html'

    @xframe_options_exempt
    def dispatch(self, request, *args, **kwargs):
        return super(APIDoctors, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        return User.objects.filter(is_private=False, is_staff=False).order_by('city')

    def get_context_data(self, **kwargs):
        context = super(APIDoctors, self).get_context_data(**kwargs)
        context['title'] = 'Наши специалисты'
        context['cities'] = City.objects.filter(user__in=User.objects.all(), user__is_staff=False,
                                                user__is_private=False).distinct().order_by('alternate_names')
        return context


class PaymentView(View):
    seminar = None
    order = None

    def dispatch(self, request, *args, **kwargs):
        self.seminar = get_object_or_404(Seminar, slug=self.kwargs.get('slug'))
        return super(PaymentView, self).dispatch(request)

    def get(self, request, *args, **kwargs):
        user = auth.get_user(request)
        try:
            old_order = Order.objects.get(user=user, object_id=self.seminar.id)
            old_order.delete()
        except ObjectDoesNotExist:
            pass

        self.order = Order.objects.create(
            user=auth.get_user(request),
            content_type=ContentType.objects.get_for_model(Seminar),
            object_id=self.seminar.id,
            amount=self.seminar.amount,
        )

        req_data = {
            'TerminalKey': TINKOFF_TERMINAL_ID,
            'Amount': int(self.order.amount * 100),
            'OrderId': self.order.id,
        }

        response = requests.post('https://securepay.tinkoff.ru/v2/Init', json=req_data)

        return HttpResponseRedirect(response.json().get('PaymentURL', '/'))


class PaymentProcessing(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(PaymentProcessing, self).dispatch(request)

    def post(self, request):
        try:
            data = json.loads(request.body.decode('utf8'))
            status = data.get('Status', 0)
            if status == Order.AUTHORIZED:
                return HttpResponse('OK', status=200)
            amount = float(data.get('Amount', 0)) / 100
            order = Order.objects.get(id=data.get('OrderId', 0))
            if status == Order.CONFIRMED:
                if order.amount >= amount:
                    order.status = Order.CONFIRMED
                    if ContentType.objects.get_for_model(Seminar) == order.content_type:
                        order.content_object.users.add(order.user)
                    elif ContentType.objects.get_for_model(PersonalPage) == order.content_type:
                        order.content_object.is_active = True
                    elif ContentType.objects.get_for_model(PersonalPageApplication) == order.content_type:
                        order.content_object.is_payment = True
                    elif ContentType.objects.get_for_model(Software) == order.content_type:
                        with pyodbc.connect(PYODBC_CONNECT) as conn:
                            cursor = conn.cursor()
                            cursor.execute("""
                            DECLARE @RC int
                            EXECUTE @RC = [dbo].[Set_Service_Payment] 1,%d
                            SELECT @RC as return_value;
                            """ % order.content_object.id_device)
                            print(order.content_object.id_device)
                            print(cursor.fetchone()[0])
                        order.content_object.is_payment = True

                    order.content_object.save()
                    order.save()
                else:
                    order.status = Order.REVERSED
                    raise Exception("Not money")
            elif status == Order.REFUNDED:
                order.status = Order.REFUNDED
                if ContentType.objects.get_for_model(Seminar) == order.content_type:
                    order.content_object.users.remove(order.user)
                elif ContentType.objects.get_for_model(PersonalPage) == order.content_type:
                    order.content_object.is_active = False
                else:
                    order.content_object.is_payment = False
                order.content_object.save()
                order.save()
            elif status == Order.REJECTED:
                order.status = Order.REJECTED
                order.save()
            return HttpResponse('OK', status=200)
        except Exception as e:
            return HttpResponse('ERROR', status=400)


class PrepaymentView(DetailView):
    template_name = 'engine/prepayment.html'
    model = PrepaymentEvent
    context_object_name = 'event'

    def get_context_data(self, **kwargs):
        context = super(PrepaymentView, self).get_context_data(**kwargs)
        context['title'] = self.object.title
        return context


class SoftwarePaymentView(TemplateView):
    template_name = 'engine/software_payment.html'
    software = None
    order = None

    def get(self, request, *args, **kwargs):
        query = request.GET
        id_device = query['id_device']
        try:
            if int(query['id_service']) == 1:
                with pyodbc.connect(PYODBC_CONNECT) as conn:
                    cursor = conn.cursor()
                    cursor.execute("""
                            SELECT COUNT(*) FROM Device WHERE UIN = %s;
                            """ % "{0:0>6}".format(id_device))
                    if cursor.fetchone()[0] == 0:
                        messages.error(request, 'Оборудование не найдено')
                        return super(SoftwarePaymentView, self).get(request, *args, **kwargs)
                self.software, created = Software.objects.get_or_create(id_device=int(id_device))
                if self.software.is_payment:
                    raise Exception('Software already payment')
                return super(SoftwarePaymentView, self).get(request, *args, **kwargs)
            raise Exception('Something wrong')
        except Exception as e:
            return HttpResponseRedirect('/')

    def post(self, request, *args, **kwargs):
        try:
            self.software = get_object_or_404(Software, id=request.POST.get('software_id', -1))

            email = request.POST.get('email', None)

            if not email:
                messages.error(request, 'Введите e-mail')
                return self.get(request, *args, **kwargs)

            self.software.email = email
            self.software.save()

            self.order = Order.objects.create(
                content_type=ContentType.objects.get_for_model(Software),
                object_id=self.software.id,
                amount=Software.get_amount(),
            )

            req_data = {
                'TerminalKey': TINKOFF_TERMINAL_ID,
                'Amount': int(self.order.amount * 100),
                'OrderId': self.order.id,
            }

            response = requests.post('https://securepay.tinkoff.ru/v2/Init', json=req_data)

            return HttpResponseRedirect(response.json().get('PaymentURL', '/'))
        except Exception:
            messages.error(request, 'Произошла ошибка')
            return self.get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(SoftwarePaymentView, self).get_context_data(**kwargs)
        context['title'] = Software.get_service_name()
        context['software'] = self.software
        return context

