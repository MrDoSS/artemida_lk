from django.conf.urls import include, url
from django.contrib import auth
import django.contrib.auth.views
from engine.accounts.forms import CustomLoginForm
from engine.chat.views import ChatView, ChatFileUpload, ChatAppView, ChatAppSuccess
from engine.search.views import SearchView
from engine import views as eviews
from engine.accounts import views as aviews

urlpatterns = [
    url(r'^$', eviews.FrontPage.as_view(), name='front_page'),
    url(r'^login/$',
        auth.views.login,
        {
            'authentication_form': CustomLoginForm,
            'extra_context':
                {
                    'title': 'Авторизация',
                }
        },
        name='login'),

    url(r'^register/$', aviews.RegView.as_view(), name='registration_register'),
    url(r"^logout/$", auth.views.logout_then_login, name="logout"),
    url(r'^', include('registration.backends.hmac.urls')),
    url(r'^account/$', aviews.AccountView.as_view(), name='account_detail',),
    url(r'^seminars/$', eviews.SeminarsView.as_view(), name='seminars'),
    url(r'^seminars/(?P<slug>[-\w\d]+)/$', eviews.SeminarDetail.as_view(), name='seminar_detail'),
    url(r'^seminars/(?P<slug>[-\w\d]+)/preview/$', eviews.SeminarPreview.as_view(), name='seminar_preview'),
    url(r'^seminars/(?P<slug>[-\w\d]+)/payment/', eviews.PaymentView.as_view(), name='seminar_payment'),
    url(r'^personal-page/$', aviews.ViewPersonalPage.as_view(), name='view_personal_page', ),
    url(r'^personal-page/create/$', aviews.CreatePersonalPage.as_view(), name='create_personal_page', ),
    url(r'^personal-page/help/$', aviews.HelpCreatePersonalPage.as_view(), name='help_create_personal_page', ),
    url(r'^personal-page/help/not-create$', aviews.PersonalPageHelpNotCreate.as_view(), name='page_help_not_create', ),
    url(r'^personal-page/payment/$', aviews.PaymentView.as_view(), name='personal_page_payment'),
    url(r'^personal-page/help/payment/$', aviews.PaymentAppView.as_view(), name='personal_page_app_payment'),
    url(r'^doctors/$', eviews.DoctorsList.as_view(), name='doctors_list', ),
    url(r'^personal-page/edit/$', aviews.PersonalUserPage.as_view(), name='personal_page'),
    url(r'^search/$', SearchView.as_view(), name='search_page'),
    url(r'^success/$', eviews.SuccessView.as_view(), name='success_page'),
    url(r'^api/get_cities/', eviews.get_cities, name='get_cities'),
    url(r'^api/get_user_city/', eviews.get_user_city, name='get_cities'),
    url(r'^api/gallery_upload/', eviews.GalleryFileUpload.as_view(), name='gallery_upload'),
    url(r'^api/doctors/', eviews.APIDoctors.as_view(), name='api_doctors'),
    url(r'^api/payment-processing/', eviews.PaymentProcessing.as_view(), name='payment_processing'),
    url(r'^chat/(?P<pk>[-\w\d]+)/', ChatView.as_view(), name='chat'),
    url(r'^chat-app/$', ChatAppView.as_view(), name='chat_app_create'),
    url(r'^chat-app/success/$', ChatAppSuccess.as_view(), name='chat_app_success'),
    url(r'^api/chat_file_upload/', ChatFileUpload.as_view(), name='chat_file_upload'),
    url(r'^landing/(?P<page_url>[-\w\d]+)/$', aviews.LandingCustomer.as_view(), name='landing'),
    url(r'^faq/$', eviews.FAQView.as_view(), name='faq'),
    url(r'^software/$', eviews.SoftwarePaymentView.as_view(), name='software'),
    url(r'^events/(?P<pk>[-\w\d]+)/$', eviews.PrepaymentView.as_view(), name='prepayment'),
]
