from django.contrib import auth
from engine.models import City, Region


def is_mine(request, obj):
    return True if obj.user == auth.get_user(request) else False


def get_filename(instance, filename):
    return filename.encode('utf-8')


def rename():
    regions = Region.objects.all()
    for reg in regions:
        index = reg.alternate_names.find(',')
        if index != -1:
            s1 = reg.alternate_names[index+1:]
            reg.alternate_names = s1
            reg.save()