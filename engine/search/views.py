from django.db.models import Q
from django.views.generic import TemplateView
from engine.accounts.models import PersonalPage, User
from engine.search.forms import SearchCities
from django.contrib.postgres.search import SearchVector


class SearchView(TemplateView):
    template_name = "engine/doctors_list.html"
    doctors = None
    search_in_inform = None
    search_in_city = None

    def get(self, request, *args, **kwargs):
        context = request.GET
        _filter = {'is_staff': False, 'is_private': False}
        self.search_in_inform = context.get('s')
        self.search_in_city = context.get('city')
        s = (Q(specialization__icontains=self.search_in_inform) |
             Q(personalpage__information__icontains=self.search_in_inform))
        if self.search_in_city:
            _filter['city'] = self.search_in_city
        self.doctors = User.objects.filter(s, **_filter)
        return super(SearchView, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(SearchView, self).get_context_data(**kwargs)
        context['doctors'] = self.doctors
        context['title'] = "Поиск"
        context['seatch_form'] = SearchCities
        return context