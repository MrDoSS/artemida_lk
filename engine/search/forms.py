from django import forms
from django.db.models import Q

from engine.accounts.models import User
from engine.models import City


class SearchCities(forms.Form):
    city = forms.ModelChoiceField(queryset=City.objects.none())

    def __init__(self, *args, **kwargs):
        cities = City.objects.filter(user__in=User.objects.all(), user__is_staff=False, user__is_private=False).distinct()
        super(SearchCities, self).__init__(*args, **kwargs)
        self.fields['city'].widget.attrs['class'] = "form-control"
        self.fields['city'].widget.attrs['id'] = "search-city"
        self.fields['city'].required = False
        self.fields['city'].queryset = cities