from django import forms
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from engine.chat.models import Chat, ChatApplication
from engine.materials.admin import AttachmentInlines
from .models import Seminar, GalleryImage, Mailing, ServiceCustomer, Testimonial, Article, Order, Category, \
    PrepaymentEvent, Software
from .accounts.models import User, PersonalPage, Education, PersonalPageApplication


class AccountEditFormAdmin(forms.ModelForm):
    class Meta:
        model = User
        exclude = ('last_login', 'groups', 'user_permissions')


class AccountAddFormAdmin(forms.ModelForm):
    class Meta:
        model = User
        exclude = ('last_login', 'groups', 'user_permissions')

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super(AccountAddFormAdmin, self).save(commit=False)
        user.set_password(self.cleaned_data["password"])
        if commit:
            user.save()
        return user


@admin.register(Seminar)
class SeminarAdmin(admin.ModelAdmin):
    list_display = ['id', 'name',  'create']
    prepopulated_fields = {'slug': ('name',)}
    filter_horizontal = ('users',)
    list_editable = ['name', 'create']
    inlines = (AttachmentInlines,)


class GalleryImageInline(admin.StackedInline):
    model = GalleryImage
    extra = 0


class ServiceCustomerInline(admin.StackedInline):
    model = ServiceCustomer
    extra = 0


class EduInline(admin.StackedInline):
    model = Education
    extra = 0


@admin.register(Testimonial)
class TestimonialAdmin(admin.ModelAdmin):
    list_display = ['author']


@admin.register(Article)
class ArticleAdmin(admin.ModelAdmin):
    list_display = ['title', 'author']


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = ['username', 'first_name', 'last_name', 'phone', 'email']
    search_fields = ['username', 'first_name', 'last_name', 'email']
    inlines = [EduInline]
    list_filter = ('is_active',)
    form = AccountEditFormAdmin
    add_form = AccountAddFormAdmin

    def get_form(self, request, obj=None, **kwargs):
        """
        Use special form during user creation
        """
        defaults = {}
        if obj is None:
            defaults['form'] = self.add_form
        defaults.update(kwargs)
        return super().get_form(request, obj, **defaults)


@admin.register(PersonalPage)
class PersonalPageAdmin(admin.ModelAdmin):
    list_display = ['user']
    search_fields = ['user']
    inlines = [ServiceCustomerInline, ]


@admin.register(Mailing)
class MailingAdmin(admin.ModelAdmin):
    list_display = ['theme', 'create']
    filter_horizontal = ('users',)

    class Media:
        js = ("admin/js/change_save_button_text.js",)


@admin.register(Chat)
class ChatAdmin(admin.ModelAdmin):
    list_display = ['name']
    filter_horizontal = ('users',)


@admin.register(ChatApplication)
class ChatAppAdmin(admin.ModelAdmin):
    list_display = ['chat', 'user']


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = ['content_object', 'amount', 'status']


@admin.register(Software)
class SoftwareAdmin(admin.ModelAdmin):
    list_display = ['name', 'email']

    def name(self, obj):
        return obj

    name.short_description = 'Название'


admin.site.register(PrepaymentEvent)
admin.site.register(PersonalPageApplication)
admin.site.register(Category)
