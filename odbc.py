import pyodbc
from config.settings import PYODBC_CONNECT

if __name__ == '__main__':
    with pyodbc.connect(PYODBC_CONNECT) as conn:
        cursor = conn.cursor()
        cursor.execute("""
        SELECT COUNT(*) FROM Device WHERE UIN = %s;
        """ % "{0:0>6}".format("150"))
        print(cursor.fetchone()[0])
